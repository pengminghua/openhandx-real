

create table REAL_AUTONUMBER
(
   TYPE                 int not null,
   EXP                  varchar(128),
   CURRNUM              decimal(32),
   DES                  varchar(1024),
   MINNUM               decimal(32),
   MAXNUM               decimal(32),
   STEP                 int,
   CURRTIME             datetime,
   LEN                  int,
   FILLCHAR             char(1),
   primary key (TYPE)
);



create table REAL_BUSLOG
(
   LOGCODE              varchar(32) not null,
   LOGTIME              date not null,
   LOGTYPE              int,
   LOGTITLE             varchar(256),
   LOGMESSAGE           varchar(4096),
   primary key (LOGCODE)
);



create table REAL_COMMONTREE
(
   TYPE                 int not null,
   BUSID                varchar(128) not null,
   PARENTID             varchar(128),
   SORT                 int not null,
   primary key (TYPE, BUSID)
);



create index IDX_TYPEPARENT on REAL_COMMONTREE
(
   TYPE
);

create index IDX_TYPELVL on REAL_COMMONTREE
(
   TYPE
);

create table REAL_ENUMTYPE
(
   TYPECODE             varchar(32) not null,
   NAME                 varchar(128) not null,
   DES                  varchar(4096),
   primary key (TYPECODE)
);



create table REAL_ENUMVALUE
(
   TYPECODE             varchar(32) not null,
   VALUE                varchar(32) not null,
   NAME1                varchar(128),
   NAME2                varchar(128),
   NAME3                varchar(128),
   NAME4                varchar(128),
   NAME5                varchar(128),
   primary key (TYPECODE, VALUE)
);



create table REAL_MESSAGE
(
   CODE                 varchar(32) not null,
   TYPE                 int not null,
   TITLE                varchar(256),
   MESSAGE1             varchar(4096),
   MESSAGE2             varchar(4096),
   MESSAGE3             varchar(4096),
   MESSAGE4             varchar(4096),
   MESSAGE5             varchar(4096),
   primary key (CODE)
);



create table REAL_SEARCHTREE
(
   TYPE                 int not null,
   BUSID                varchar(128) not null,
   LVL                  int not null,
   LEFTSTEP             int not null,
   RIGHTSTEP            int not null,
   primary key (TYPE, BUSID)
);



create index IDX_TYPELEFTLVL on REAL_SEARCHTREE
(
   TYPE,
   LEFTSTEP,
   LVL
);

create index IDX_TYPERIGHTLVL on REAL_SEARCHTREE
(
   TYPE,
   RIGHTSTEP,
   LVL
);

create index IDX_TYPELEFTRIGHTLVL on REAL_SEARCHTREE
(
   TYPE,
   LEFTSTEP,
   RIGHTSTEP,
   LVL
);

create index IDX_TYPELVL on REAL_SEARCHTREE
(
   TYPE,
   LVL
);

